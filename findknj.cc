/*

  Sanyo Medicom MC-770-10GT HDD image dump string finder
  2015 H.Tomari. Public Domain.

  MC-770-10GT system disk contains strings encoded in JIS X0201 and JIS X0208
  with escape sequence 1B AD to switch to Kanji mode and 1B AE to switch back
  to ANK mode. This program extracts strings and converts them to ISO-2022-JP-2
  encoding.

  Example:
   % dd if=/dev/sdb bs=512 conv=swab,noerror,sync > dump.img
   % ./findknj dump.img | iconv -c -f iso-2022-jp-2 -t utf-8 > strings.txt
   % less strings.txt

*/
#include <memory>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <locale>

static int ishankaku(unsigned char c) {
	return (0xa0 <= c) && (c <= 0xdf);
}

class Linebuf {
public:
	Linebuf(long size);
	long append_printable(char c);
	long append_katakana(char c);
	long append_kanji(char c);
	long length() { return m_pos; }
	void flush();
private:
	void check_overflo();
	std::shared_ptr<char> m_line;
	std::shared_ptr<char> m_attrib;
	long m_size;
	long m_pos;
};

Linebuf::Linebuf(long size):
	m_line(std::shared_ptr<char>(new char[size])),
	m_attrib(std::shared_ptr<char>(new char[size])),
	m_size(size),
	m_pos(0l)
{
}
void
Linebuf::check_overflo() {
	if(m_pos>=m_size-1l) { throw std::runtime_error("line buffer overflow"); }
}
long
Linebuf::append_printable(char c) {
	check_overflo();
	m_line.get()[m_pos]=c;
	m_attrib.get()[m_pos++]='p';
	return m_pos;
}
long
Linebuf::append_katakana(char c) {
	check_overflo();
	m_line.get()[m_pos]=c;
	m_attrib.get()[m_pos++]='k';
	return m_pos;
}
long
Linebuf::append_kanji(char c) {
	check_overflo();
	m_line.get()[m_pos]=c;
	m_attrib.get()[m_pos++]='K';
	return m_pos;
}
void
Linebuf::flush() {
	if(m_pos>=5) {
		char last_attr='p';
		char *lin=m_line.get();
		char *attr=m_attrib.get();
		for(long i=0; i<m_pos; i++) {
			if(attr[i]=='p') {
				if(last_attr!='p') {
					std::cout << "\033(B";
				}
				std::cout << lin[i];
			} else if(attr[i]=='k') {
				if(last_attr!='k') {
					std::cout << "\033(I";
				}
				char katakana_7b=0x7f&lin[i];
				std::cout << katakana_7b;
			} else if(attr[i]=='K') {
				if(last_attr!='K') {
					std::cout << "\033$B";
				}
				std::cout << lin[i];
			}
			last_attr=attr[i];
		}
		if(last_attr!='p') {
			std::cout << "\033(B";
		}
		std::cout << '\n';
	}
	m_pos=0;
}

extern int main(int argc, char *argv[]) {
	unsigned state=0u;
	Linebuf lbuf(32768l);
	
	std::ifstream inFile;
	inFile.open(argv[1],std::ifstream::in | std::ifstream::binary);
	
	char c;
	while(inFile.get(c)) {
		if(state==0u) {
			if(c==0x1b) { state=1u; }
			else if(ishankaku((unsigned char)c)) {
				lbuf.append_katakana(c);
			} else if(std::isprint((unsigned char)c)) {
				lbuf.append_printable(c);
			} else {
				lbuf.flush();
			}
		} else if(state==1u) {
			if((unsigned char)c==0xad) { 
				state=2u;
			} else {
				state=0u;
				lbuf.flush();
			}
		} else if(state==2u) {
			if(0x21<=c && c<=0x7e) {
				lbuf.append_kanji(c);
			} else if(c==0x1b) {
				state=3u;
			} else {
				lbuf.flush();
				state=0u;
			}
		} else if(state==3u) {
			if((unsigned char)c==0xae) {
				state=0u;
			} else {
				lbuf.flush();
				state=0u;
			}
		}
	}
	inFile.close();
	return EXIT_SUCCESS;
}
