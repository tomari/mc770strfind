# Sanyo Medicom MC-770-10GT HDD image dump string finder

Public Domain.

Sanyo MC-770-10GT system disk contains strings encoded in JIS X0201 and JIS X0208
with escape sequence 1B AD to switch to Kanji mode and 1B AE to switch back
to ANK mode. This program extracts strings and converts them to ISO-2022-JP-2
encoding.

##Example

``` 
 % dd if=/dev/sdb bs=512 conv=swab,noerror,sync > dump.img
 % ./findknj dump.img | iconv -c -f iso-2022-jp-2 -t utf-8 > strings.txt
 % less strings.txt
```
